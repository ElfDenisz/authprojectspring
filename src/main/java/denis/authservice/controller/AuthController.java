package denis.authservice.controller;

import denis.authservice.dto.JWTResponseDto;
import denis.authservice.dto.TokenDto;

import denis.authservice.service.LogoutService;
import denis.authservice.service.SignUpService;
import denis.authservice.service.SingInService;
import denis.authservice.service.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import de.insurance.model.SignupRequest;
import de.insurance.model.SignupResponse;
import de.insurance.model.SinginRequest;
import de.insurance.model.SinginResponse;
import jakarta.validation.Valid;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;


@RestController
@RequestMapping(path = "api/v1/auth")
@RequiredArgsConstructor
public class AuthController {
    private final SignUpService signUpService;
    private final SingInService singInService;
    private final LogoutService logoutService;
    private final TokenService tokenService;


    @PostMapping("/login")
    public ResponseEntity<SinginResponse> loginUserz(@RequestBody @Valid SinginRequest singinRequest) {
        SinginResponse singinResponse = singInService.signIn(singinRequest);
        return ResponseEntity.ok(singinResponse);
    }


    @PostMapping(path = "/singup")
    public ResponseEntity<SignupResponse> registerUser(@RequestBody @Valid SignupRequest signUpRequest) {
        SignupResponse response = signUpService.signUp(signUpRequest);
        return ResponseEntity.ok(response);
    }

    @GetMapping(path = "/logout")
    public ResponseEntity<Void> logoutUser(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
      logoutService.logout(httpServletRequest, httpServletResponse, authentication);
      return ResponseEntity.noContent().build();
    }

    @GetMapping(path = "/testlogout")
    public ResponseEntity<String> testlogoutUser(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) {
       String username = logoutService.testlogout(httpServletRequest, httpServletResponse, authentication);
        return ResponseEntity.ok(username);
    }


    @PostMapping("/refreshTokens")
    public ResponseEntity<JWTResponseDto> getNewAccessToken(@RequestBody TokenDto refreshToken) {
       var value = refreshToken;
       var name = refreshToken.getRefreshToken();
        System.out.println(name);
        return ResponseEntity.ok(tokenService.refreshTokens(refreshToken.getRefreshToken()));
    }

}
