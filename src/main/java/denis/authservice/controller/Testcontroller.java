package denis.authservice.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(path = "test")
@RequiredArgsConstructor
public class Testcontroller {

    @GetMapping(path = "/test1")
    public String test() {
        String valus = "string";
        valus += "s";
        return valus;
    }
}
