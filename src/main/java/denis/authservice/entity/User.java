package denis.authservice.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "uzers")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "firstname", nullable = false)
    private String firstname;

    @Column(name = "lastname", nullable = false)
    private String lastname;
    @Column(name = "username", nullable = false)
    private String username;
    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "birthDate", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private OffsetDateTime birthDate;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USER;
    @Column(name = "createdDate", updatable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private OffsetDateTime createdDate;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    private List<Token> tokens = new ArrayList<>();

    public User(String firstname, String lastname, String username, String email, String password, OffsetDateTime birthDate, String address, Role role, OffsetDateTime createdDate) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.email = email;
        this.password = password;
        this.birthDate = birthDate;
        this.address = address;
        this.role = role;
        this.createdDate = createdDate;
    }

    public User() {
    }

    @PrePersist
    protected void onCreate() {
        this.createdDate = OffsetDateTime.now();
    }

}
