package denis.authservice.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.OffsetDateTime;

@Data
@Entity
@Table(name = "tokens")
public class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "token")
    private String token;
    @ManyToOne
    @JoinColumn(name = "uzer_id", referencedColumnName = "id")
    private User user;
    @Column(name = "expired_at")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private OffsetDateTime expiredAt;

    @Column(name = "role", nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.STRING)
    private Type tokenType;

    public Token(String token) {
        this.token = token;
    }

    public Token() {
    }
}
