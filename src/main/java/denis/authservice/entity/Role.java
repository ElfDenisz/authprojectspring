package denis.authservice.entity;

public enum Role {
    USER, ADMIN, AGENT
}
