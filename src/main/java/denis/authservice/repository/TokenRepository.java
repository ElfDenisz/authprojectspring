package denis.authservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import denis.authservice.entity.Token;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {

    List<Token> findTokensByUserId(Long id);

    Token findTokenByToken(String token);

    List<Token> findTokensByUserUsername(String username);



    @Query(nativeQuery = true, value = "select token t FROM Tokens join Uzers u where u.name = :name and t.type =:type")
    Token findAccessTokenByUserName(@Param("name") String name, @Param("type") String type);


}
