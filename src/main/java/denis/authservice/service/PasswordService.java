package denis.authservice.service;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PasswordService {
    private final PasswordEncoder passwordEncoder;

    public String encodePassword(String rawPass) {
        return passwordEncoder.encode(rawPass);
    }

    public boolean validPassword(String rawPass, String decodedPass) {
        return passwordEncoder.matches(rawPass, decodedPass);
    }

}
