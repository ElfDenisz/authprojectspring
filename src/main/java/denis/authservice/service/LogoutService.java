package denis.authservice.service;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;

import lombok.RequiredArgsConstructor;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LogoutService implements LogoutHandler {
    private final TokenService tokenService;
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
           tokenService.revolkAllUsersTokens(request);
    }

    public String testlogout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
      return tokenService.getUserNameFromRequest(request);
    }
    }
