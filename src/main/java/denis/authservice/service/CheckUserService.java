package denis.authservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import denis.authservice.repository.UserRepository;

@Service
public class CheckUserService {

    UserRepository userRepository;

    @Autowired
    public CheckUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public Boolean checkIfExistUserByUsername(String username) {
        return userRepository.existsUserByUsername(username);
    }

    public Boolean checkIfExistUserByEmail(String email) {
        return userRepository.existsUserByEmail(email);
    }
}
