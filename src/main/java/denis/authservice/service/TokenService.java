package denis.authservice.service;

import java.time.OffsetDateTime;
import java.util.List;

import denis.authservice.dto.JWTResponseDto;
import denis.authservice.entity.Type;
import denis.authservice.entity.Token;
import denis.authservice.entity.User;
import denis.authservice.exception.AccessException;
import denis.authservice.repository.TokenRepository;
import denis.authservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.transaction.annotation.Transactional;


@Service
//@RequiredArgsConstructor
public class TokenService {

    private final TokenValidationServise tokenValidationServise;
    private final TokenRepository tokenRepository;
    private final UserRepository userRepository;
    private OffsetDateTime liveTimeForAccessToken;
    private OffsetDateTime liveTimeForForRefreshToken;
    private String key;


    public TokenService(TokenValidationServise tokenValidationServise,
                        TokenRepository tokenRepository,
                        UserRepository userRepository,
                        @Value("${jwt_secret:SECRET}") String key,
                        @Value("${jwt.access.expiredInHours:12}") Integer amountOfHoursForAccessTokenExpired,
                        @Value("${jwt.refresh.expiredInDays:30}") Integer amountOfDaysWhenRefreshTokenExpired
    ) {
        this.tokenValidationServise = tokenValidationServise;
        this.tokenRepository = tokenRepository;
        this.userRepository = userRepository;
        this.key = key;
        this.liveTimeForAccessToken = OffsetDateTime.now().plusHours(amountOfHoursForAccessTokenExpired);
        this.liveTimeForForRefreshToken = OffsetDateTime.now().plusDays(amountOfDaysWhenRefreshTokenExpired);
    }

    public Token generateAccessToken(User user) {
        var newAccessToken = JWT.create()
                .withClaim("username", user.getUsername())
                .sign(Algorithm.HMAC256(key));
        var accesToken = new Token(newAccessToken);
        accesToken.setExpiredAt(liveTimeForAccessToken);
        accesToken.setRole(user.getRole());
        accesToken.setTokenType(Type.ACCESS);
        accesToken.setUser(user);
        tokenRepository.save(accesToken);
        return accesToken;
    }

    public void linkAccessToken(Token token, User user) {
        user.getTokens().add(token);
        userRepository.save(user);
    }

    public Token generateRefreshToken(User user) {
        var newRefreshToken = JWT.create()
                .withSubject("User")
                .withClaim("username", user.getUsername())
                .withExpiresAt(liveTimeForForRefreshToken.toInstant())
                .sign(Algorithm.HMAC256(key));
        var refreshToken = new Token(newRefreshToken);
        refreshToken.setRole(user.getRole());
        refreshToken.setTokenType(Type.REFRESH);
        refreshToken.setExpiredAt(liveTimeForForRefreshToken);
        refreshToken.setUser(user);
        tokenRepository.save(refreshToken);
        return refreshToken;
    }

    public String getUserNameFromRequest(HttpServletRequest request) throws JWTVerificationException {
        String authHeader = request.getHeader("Authorization");
        if (authHeader != null && !authHeader.isBlank() && authHeader.startsWith("Bearer ")) {
            String token = authHeader.substring(7);
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(key))
                    .withSubject("User")
                    .build();
            DecodedJWT decodedJWT = verifier.verify(token);
            return decodedJWT.getClaim("username").asString();
        } else {
            throw new AccessException("Invalid JWT Token");
        }
    }


    public void revolkAllUsersTokens(HttpServletRequest request) {
        User user = userRepository.findByUsername(getUserNameFromRequest(request));
        if (user != null) {
            List<Token> tokens = tokenRepository.findTokensByUserId(user.getId());
            if (tokens != null) {
                tokens.forEach(token -> tokenRepository.deleteById(token.getId()));
            }
        }

    }

    public boolean validateAccessToken(String accessToken) {
        return validateToken(accessToken);
    }

    public boolean validateRefreshToken(String refreshToken) {
        return validateToken(refreshToken);
    }

    private boolean validateToken(String token) {
        Token validatedtoken = tokenRepository.findTokenByToken(token);

        if (validatedtoken != null && validatedtoken.getExpiredAt().isAfter(OffsetDateTime.now())) {
            return true;
        }
        return false;

    }

    private String getUserNameFromToken(String token) {
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(key))
                .withSubject("User")
                .build();
        DecodedJWT decodedJWT = verifier.verify(token);
        return decodedJWT.getClaim("username").asString();
    }
@Transactional
    public JWTResponseDto refreshTokens(String refreshToken) {
        if (validateRefreshToken(refreshToken)) {
            var user = userRepository.findByUsername(getUserNameFromToken(refreshToken));
            List<Token> tokens = tokenRepository.findTokensByUserId(user.getId());
            if (tokens != null) {
                tokens.forEach(token -> tokenRepository.deleteById(token.getId()));
            }
          //  tokenRepository.delete(tokenRepository.findTokenByToken(refreshToken));
            return new JWTResponseDto(generateAccessToken(user).getToken(), generateRefreshToken(user).getToken());

        }
        else {
            throw new AccessException("wrong refreshToken");
        }
    }





}



