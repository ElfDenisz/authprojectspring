package denis.authservice.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import denis.authservice.exception.InvalidPasswordExeption;
import denis.authservice.security.UserDetailsImpl;

import de.insurance.model.SinginRequest;
import de.insurance.model.SinginResponse;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SingInService {

    private final UserDetailsService userDetailsService;
    private final TokenService tokenService;

    private final PasswordService passwordService;

    @Transactional
    public SinginResponse signIn(SinginRequest singinRequest) {

        UserDetails userDetails = userDetailsService.loadUserByUsername(singinRequest.getEmail());
        var passFromRequest = singinRequest.getPassword();
        var passFromDB = userDetails.getPassword();

        if (!passwordService.validPassword(passFromRequest, passFromDB)) {
            throw new InvalidPasswordExeption("passwords don't match");
        }
        var user = ((UserDetailsImpl) userDetails).getUser();
        var accessToken = tokenService.generateAccessToken(user);
        tokenService.linkAccessToken(accessToken, user);

        var refreshToken = tokenService.generateRefreshToken(((user)));
        tokenService.linkAccessToken(refreshToken, user);
        return new SinginResponse().token(accessToken.getToken()).refreshToken(refreshToken.getToken());
    }
}
