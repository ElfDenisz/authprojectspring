package denis.authservice.service;

import denis.authservice.entity.User;
import denis.authservice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

}
