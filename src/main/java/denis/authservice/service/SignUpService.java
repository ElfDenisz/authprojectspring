package denis.authservice.service;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import denis.authservice.entity.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import denis.authservice.entity.Role;
import denis.authservice.repository.UserRepository;

import de.insurance.model.SignupRequest;
import de.insurance.model.SignupResponse;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class SignUpService {

    private final ValidationService validationService;
    private final UserRepository userRepository;
    private final PasswordService passwordService;
    private final TokenService tokenService;


    @Transactional
    public SignupResponse signUp(SignupRequest singupRequest) {
        validationService.checkRequest(singupRequest);
        User user = new User(singupRequest.getFirstname(), singupRequest.getLastname(), singupRequest.getUsername(),
            singupRequest.getEmail(), passwordService.encodePassword(singupRequest.getPassword()),
            singupRequest.getBirthDate().atStartOfDay(ZoneOffset.UTC).toOffsetDateTime(), singupRequest.getAddress(), Role.USER,
            OffsetDateTime.now());

        var accessToken = tokenService.generateAccessToken(user);
        var refreshToken = tokenService.generateRefreshToken(user);
        user.getTokens().add(accessToken);
        user.getTokens().add(refreshToken);
        userRepository.save(user);
        return new SignupResponse().token(accessToken.getToken()).refreshToken(refreshToken.getToken());
    }
}
