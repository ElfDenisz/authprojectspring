package denis.authservice.service;

import denis.authservice.exception.AccessException;
import de.insurance.model.SinginRequest;
import org.springframework.stereotype.Service;

import denis.authservice.exception.UserAlreadyExistException;

import de.insurance.model.SignupRequest;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ValidationService {
    private final CheckUserService checkUserService;

    public void checkRequest(SignupRequest singupRequest) {
        if (checkUserService.checkIfExistUserByEmail(singupRequest.getEmail())) {
            throw new UserAlreadyExistException("User with email %s already exists".formatted(singupRequest.getEmail()));
        }
        if (checkUserService.checkIfExistUserByUsername(singupRequest.getUsername())) {
            throw new UserAlreadyExistException("User with username %s already exists".formatted(singupRequest.getUsername()));
        }
    }
    public void checkLoginRequest(SinginRequest singinRequest) {
        if (!checkUserService.checkIfExistUserByEmail(singinRequest.getEmail())) {
            throw new AccessException("User with email %s not exists".formatted(singinRequest.getEmail()));
        }
    }

}
