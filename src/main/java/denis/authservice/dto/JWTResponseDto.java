package denis.authservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class JWTResponseDto {
    private String accessToken;
    private String refreshToken;

    
}
