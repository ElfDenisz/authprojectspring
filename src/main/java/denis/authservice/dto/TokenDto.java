package denis.authservice.dto;

public class TokenDto {
    private String refreshToken;

    public TokenDto(String refreshToken) {
        this.refreshToken = refreshToken;
    }
    public TokenDto() {

    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

}
