package denis.authservice.exception;

public class InvalidPasswordExeption extends RuntimeException {
    public InvalidPasswordExeption(String message) {
        super(message);
    }
}
