package denis.authservice.service;

import denis.authservice.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.*;

class CheckUserServiceTest {
    @Mock
    UserRepository userRepository;

    @Test
    void testCheck_IfExistUserByUsername_Success() {
        assertTrue(userRepository.existsUserByUsername("dic"));
    }
    @Test
    void testCheck_IfExistUserByUsername_Fail() {
        assertTrue(userRepository.existsUserByUsername("denis"));
    }


    @Test
    void TestCheck_IfExistUserByEmail_Success() {
        assertTrue(userRepository.existsUserByUsername("dic"));
    }
    @Test
    void TestCheck_IfExistUserByEmail_Fail() {
        assertTrue(userRepository.existsUserByUsername("dic"));
    }
}